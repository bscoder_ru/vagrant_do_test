#
# Cookbook Name:: deploy
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

user "deploy" do
  home "/home/deploy"
  shell "/bin/bash"
  action :create
  supports :manage_home => true
end

directory "/home/deploy/.ssh" do
  owner "deploy"
  mode "0700"
  action :create
end

file "/home/deploy/.ssh/authorized_keys" do 
  owner "deploy"
  mode "0600"
  action :create
  content <<EOF
ssh-dss AAAAB3NzaC1kc3MAAACBAKZp2COWkjaAGbI1caqxAw1SvXDYtRRTunsyKWe4XX4gPIhdq21cIOFu+6hWnYP86jFzuyh4kYhD08F7YIOf6AYbzCD/rWtoF3xD3gPgR5+vvIQDybPijwxOXmhmn2/mlZwJJyhZufX1gQyxGSSVUFMaetQpLuirMS/fXWjJnEVFAAAAFQClmRhdOEFGsMJCxFmgpkwgiFoYpwAAAIADwC7nsQsdxMSoSKY1nT1OtFUyC4fM+Y1sbKeG7VVz4g1bGMDlRZSis8Oe83yA292wkwv5Z1/ECCvytXsGfazCzGcQ0ryBB0tN7ujjCCOxMW1/eBvVpf9mPiwAC1HV8gkt2wY7PzID7HiSJyU0+YmGj6f7xJU1uDmoJqr/cC2wDwAAAIEAmTG8tqWGrfh/PoiC3hRdTLxzR6UHjwUfXPhPG+P29+JgHgoZzPCNPI1hlkhGVTbROnAxoyqMYywXXzKCx+JavZfoMpN3mgJ1f09kKzUxX9+3Txdg+iWmXo373/xTRJHZ/0eq46YcEq9hGLPyxFG3LCZFMDOj+Ub4QIbDab8JY9Y= bscoder@bsmain
EOF
end
