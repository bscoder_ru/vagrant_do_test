$cp config/digitalocean.yml.example config/digitalocean.yml

$vim config/digitalocean.yml

$vagrant up ocean1 --provider=digital_ocean

$vagrant ssh ocean1
