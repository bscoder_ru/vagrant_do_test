# -*- encoding: utf-8 -*-
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.require_plugin('vagrant-omnibus')
Vagrant.require_plugin('vagrant-vbguest')
Vagrant.require_plugin('vagrant-digitalocean')
#Vagrant.require_plugin('vagrant-berkshelf')

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.ssh.forward_agent = true
  config.berkshelf.enabled = true
  config.omnibus.chef_version = "11.4.0"
  if File::exists?("config/digitalocean.yml")
    %w(ocean1 ocean2 ocean3 ocean4).each do |boxname|
      config.vm.define(boxname) do |ocean|
        ocean.vm.synced_folder '.', '/vagrant', :disabled => true
        ocean.vm.provider :digital_ocean do |provider, override|
          override.vm.box = 'rails-do'
          override.vm.hostname = "rails-sandbox-#{boxname}"
          override.vm.box_url = "https://github.com/smdahlen/vagrant-digitalocean/raw/master/box/digital_ocean.box"
          override.ssh.private_key_path = '~/.ssh/id_dsa'

          begin
            auth = YAML.load_file('config/digitalocean.yml')
            provider.client_id = auth['client_id']
            provider.api_key = auth['api_key']
            provider.image = auth.fetch('image',"Ubuntu 12.04.4 x32")
            provider.region = auth.fetch('region', "Amsterdam 2")
            provider.size = auth.fetch('size', "512MB")
          rescue
            puts "error in auth info (config/digitalocean.yml)"
            exit
          end
          provider.ca_path = ENV['SSL_CERT_FILE']
        end

        ocean.vm.provision :chef_solo do |chef|
          chef.add_recipe :apt
          chef.add_recipe "deploy"         
          chef.add_recipe "rvm::user"
          chef.json = {
            rvm: {
              user_autolibs: "read-fail",
              user_installs: [ 
                              { user: "deploy",
                                default_ruby: "2.0.0-p451@rails",
                                rubies: ["2.0.0-p451"]
                              }
                             ]
            },
          }
        end
      end
    end
  end
end
